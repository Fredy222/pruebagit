/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebagit.Controller;

/**
 *
 * @author fredy.romerousam
 */
import com.diagnostico.Entity.Producto;
import com.pruebagit.dao.ProductoDAO;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@SessionScoped
@ManagedBean
public class ProductoControl {

    public ProductoControl() {
    }
    ProductoDAO pro=new ProductoDAO();
    public Producto producto;
    public List<Producto> listaP;

    public ProductoDAO getPro() {
        return pro;
    }

    public void setPro(ProductoDAO pro) {
        this.pro = pro;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public List<Producto> getListaP() {
        listaP=pro.listarP();
        return listaP;
    }

    public void setListaP(List<Producto> listaP) {
        this.listaP = listaP;
    }
    
    public void init(){
    pro=new ProductoDAO();
      }    
}
