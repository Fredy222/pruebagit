/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebagit.Controller;

import com.diagnostico.Entity.Cliente;
import com.pruebagit.dao.ClienteDAO;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author fredy.romerousam
 */
@SessionScoped
@ManagedBean
public class ClienteControl {

    Cliente c;
    List<Cliente> listaC;

    ClienteDAO cdao = new ClienteDAO();

    public Cliente getC() {
        return c;
    }

    public void setC(Cliente c) {
        this.c = c;
    }

    public List<Cliente> getListaC() {
       listaC=cdao.listarCli();
        return listaC;
    }

    public void setListaC(List<Cliente> listaC) {
        this.listaC = listaC;
    }

    public ClienteDAO getCdao() {
        return cdao;
    }

    public void setCdao(ClienteDAO cdao) {
        this.cdao = cdao;
    }

    public void init() {
        c = new Cliente();
    }
    
    
}
