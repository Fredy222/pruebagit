/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebagit.Conexion;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author fredy.romerousam
 */
public class Conexion {

    private static String db = "participante3";
    private static String user = "root";
    private static String pass = "root";
    private static String ruta = "jdbc:mysql://localhost:3306/"+db+"?useSSL=false";
    public Connection conn;

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public Connection conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(ruta, user, pass);
            if(conn!=null){
                System.out.println("Exitos!!!!!");
            }
            return conn;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void desconectar() {
        try {
            conn.close();
        } catch (Exception e) {
        }
    }

}
