/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebagit.dao;

/**
 *
 * @author fredy.romerousam
 */
import com.diagnostico.Entity.Cliente;
import com.pruebagit.Conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAO {

    Conexion conn = new Conexion();

    public List<Cliente> listarCli() {
        List<Cliente> listaCX = new ArrayList<>();
        try {
            String sql = "Select * From Cliente";
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Cliente c = new Cliente();
                c.setId(rs.getInt("id"));
                c.setNombre(rs.getString("nombre"));
                c.setApellido(rs.getString("apellido"));
                c.setDireccion(rs.getString("direccion"));
                c.setFechaNacimiento(rs.getString("fecha_nacimiento"));
                c.setTelefono(rs.getString("telefono"));
                c.setEmail(rs.getString("email"));
                listaCX.add(c);
            }
            return listaCX;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
