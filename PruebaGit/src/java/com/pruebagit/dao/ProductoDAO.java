/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebagit.dao;

import com.pruebagit.Conexion.Conexion;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.diagnostico.Entity.Producto;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fredy.romerousam
 */
public class ProductoDAO {

    Conexion conn = new Conexion();

    public List<Producto> listarP() {
        List<Producto> lista = new ArrayList<>();
        try {
            String sql = "Select * from producto";
            PreparedStatement ps = conn.conectar().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Producto p = new Producto();
                p.setId(rs.getInt("id"));
                p.setNombre(rs.getString("nombre"));
                p.setPrecio(rs.getBigDecimal("precio"));
                p.setTock(rs.getInt("tock"));
                lista.add(p);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
}
